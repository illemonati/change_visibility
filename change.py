import gitlab


def change(token: str):
    gl = gitlab.Gitlab('https://www.gitlab.com', private_token=token)
    gl.auth()
    for project in gl.projects.list(owned=True, all=True):
        print("changed : {}".format(project.name))
        try:
            change_to_public(project)
        except Exception as e:
            print(e)


def change_to_public(project):
    project.visibility = 'public'
    project.save()


def change_to_private(project):
    project.visibility = 'private'
    project.save()


if __name__ == '__main__':
    change('')
